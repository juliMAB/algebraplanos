﻿using UnityEngine;



    public static class RendererExtension 
    {

        public static bool IsVisibleFrom(this Renderer renderer, Camera cam)
        {
            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(cam);
            return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
        }
    }


