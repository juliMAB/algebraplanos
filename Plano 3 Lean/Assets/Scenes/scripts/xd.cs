﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class xd : MonoBehaviour
{
    public Camera cam;
    private void Update()
    {
        if (RendererExtension.IsVisibleFrom(GetComponent<MeshRenderer>(),cam))
        {
            transform.localScale = Vector3.one;
            GetComponent<MeshRenderer>().forceRenderingOff = false;
        }
        else
        {
            GetComponent<MeshRenderer>().forceRenderingOff = true;
        }
    }
}


