﻿using System;
using UnityEngine;
using System.Collections;

public class ExampleClass : MonoBehaviour
{
    private GameObject[] go = new GameObject[6];
    void Start()
    {
        // Calculate the planes from the main camera's view frustum
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);

        // Create a "Plane" GameObject aligned to each of the calculated planes
        for (int i = 0; i < 6; ++i)
        {
            GameObject p = GameObject.CreatePrimitive(PrimitiveType.Plane);
            p.name = "Plane " + i.ToString();
            p.transform.position = -planes[i].normal * planes[i].distance;
            p.transform.rotation = Quaternion.FromToRotation(Vector3.up, planes[i].normal);
            go[i] = p;
        }
    }

    private void Update()
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
        for (int i = 0; i < 6; i++)
        {
            go[i].transform.position = -planes[i].normal * planes[i].distance;
            go[i].transform.rotation = Quaternion.FromToRotation(Vector3.up, planes[i].normal);
        }
    }
}