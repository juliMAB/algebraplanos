﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

public class MyCamaraClass : MonoBehaviour
{
    public float Near;

    public float Far;

    private void OnValidate()
    {
        if (Near < 0)
        {
            Near = 0;
        }

        if (Near > Far)
        {
            Far = Near;
        }
    }

    private Plane examPlane;
    private GameObject piso;
    private GameObject frontal;
    private GameObject atras;
    private GameObject derecha;
    private GameObject izquierda;
    private GameObject techo;
    private GameObject middle;
    void Start()
    {
        //crar todos los planos para el frustrum.
        piso = GameObject.CreatePrimitive(PrimitiveType.Plane);
        frontal = GameObject.CreatePrimitive(PrimitiveType.Plane);
        atras = GameObject.CreatePrimitive(PrimitiveType.Plane);
        derecha = GameObject.CreatePrimitive(PrimitiveType.Plane);
        izquierda = GameObject.CreatePrimitive(PrimitiveType.Plane);
        techo = GameObject.CreatePrimitive(PrimitiveType.Plane);
        middle = Instantiate(new GameObject(),transform );
        //les pongo nombre porque sino es re complicado.
        piso.gameObject.name = "piso";
        frontal.gameObject.name = "frontal";
        atras.gameObject.name = "atras";
        derecha.gameObject.name = "derecha";
        izquierda.gameObject.name = "izquierda";
        techo.gameObject.name = "techo";
        middle.gameObject.name = "PuntoMedio";
        //los hago todos hijos de esta camara.
        var transform1 = this.transform;
        var parent = transform1;
        piso.transform.parent = parent;
        frontal.transform.parent = parent;
        atras.transform.parent = parent;
        derecha.transform.parent = parent;
        izquierda.transform.parent = parent;
        techo.transform.parent = parent;
        //seteo los planos de distancia en relacion a mi camara.
        var position = transform1.position;
        frontal.transform.localPosition = Vector3.forward*Near;
        atras.transform.localPosition = Vector3.forward * Far;
        middle.transform.localPosition = frontal.transform.position - atras.transform.position;
        //hago que las normales de los planos apunten a el centro que saco con los 2 planos.
        frontal.transform.localRotation = Quaternion.Euler(90, 0, 0);
        atras.transform.localRotation = Quaternion.Euler(-90,0,0);

    }

    // Update is called once per frame
    void Update()
    {
        //DatosUpdate();
    }

    void DatosUpdate()
    {
        frontal.transform.localPosition = transform.position + Vector3.forward * Near;
        atras.transform.localPosition = transform.position + Vector3.forward * Far;
        middle.transform.localPosition = frontal.transform.position - atras.transform.position;
    }
}
